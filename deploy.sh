#!/bin/bash -e

ENV=$1

if [[ ${ENV} != "uat" && ${ENV} != "prod" ]]; then
  echo "Invalid environment"
  exit 1
fi

# Fetch the secrets from OpenShift, to be injected into the template
# In the future they could be fetched from another system like Vault
# Or ideally, OpenShift should allow to reference secrets in Route definitions (not the case)
tls_cert_pem=$(oc get secret public-tls-cert --template='{{.data.pem}}' | base64 -d)
tls_cert_key=$(oc get secret public-tls-cert --template='{{.data.key}}' | base64 -d)
tls_cert_chain=$(oc get secret public-tls-cert --template='{{.data.chain}}' | base64 -d)

oc process -f openshift/template.yaml \
  --param-file params/${ENV}.ini \
  --param TLS_CERT_PEM=${tls_cert_pem} \
  --param TLS_CERT_KEY=${tls_cert_key} \
  --param TLS_CERT_CHAIN=${tls_cert_chain} \
  | oc apply -f -
